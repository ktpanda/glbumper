fog{
    color rgbt <.7,.7,.7,.5>
    fog_type 2
    fog_alt 0.5
    fog_offset 0
    distance 1.5
    turbulence <.15, .15, .15>
    omega 0.35
    lambda 1.25
    octaves 5
}
